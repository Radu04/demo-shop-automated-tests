## RO
# Demo Shop Test Automation Framework
Prin intermediul acestui proiect se realizează testarea automatizată pentru Demo Shop. Acest proiect este structurat în mai multe tipuri de clase cu diverse scopuri, cum ar fi constante, obiecte de date și teste. Pentru a avea proiectul cât mai curat și organizat, testele sunt împărțite în mai multe clase, fiecare reprezentând o zonă a site-ului.


## Acesta este proiectul final pentru Radu Muresan, in cadrul cursului de "TESTARE SOFTWARE".

### Student: _Radu Florin Muresan_

### IDE: Intellij IDEA

### Tehnologii folosite:
    - Java17
    - Maven
    - Selenide Framework
    - PageObject Models
    - TestNG
    - DataProvider
    - Allure


### Cum să rulezi testele
`git clone https://gitlab.com/Radu04/demo-shop-automated-tests.git`

Executați următoarele comenzi pentru:
#### Executați toate testele
- `mvn clean tests`
#### Generează raport
- `mvn allure:report`
#### Deschideți și prezentați raportul
- `mvn allure:serve`

#### Page Objects
    - HomePage
    - Header
    - Footer
    - CartPage
    - Product
    - LoginModal
    - CheckoutInfoPage

#### Test Classes
    - DemoShopTests
    - CartTests
    - LoginTests
    - CheckoutInfoTests


## EN
# Demo Shop Test Automation Framework
This project represents an automation testing framework for the Demo Shop. This project is structured in multiple types of classes with various purposes like constants, data objects and tests. In order to have the project as clean and organized as possible the tests are split up in multiple classes, each representing an area of the website.


## This is the final project for Radu Muresan, within the "QUALITY ASSURANCE" course.

### Student: _Radu Florin Muresan_

### IDE: Intellij IDEA

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framework
    - PageObject Models
    - TestNG
    - DataProvider
    - Allure


### How to run the tests
`git clone https://gitlab.com/Radu04/demo-shop-automated-tests.git`

Execute the following commands to:
#### Execute all tests
- `mvn clean tests`
#### Generate Report
- `mvn allure:report`
#### Open and present report
- `mvn allure:serve`

#### Page Objects
    - HomePage
    - Header
    - Footer
    - CartPage
    - Product
    - LoginModal
    - CheckoutInfoPage

#### Test Classes
    - DemoShopTests
    - CartTests
    - LoginTests
    - CheckoutInfoTests
