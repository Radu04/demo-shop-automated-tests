package org.fasttrackit.Tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Cart features")
public class CartTests {

    Header header;
    CartPage cartPage;

    @BeforeTest
    public void before() {
        AllureSelenide listener = new AllureSelenide().screenshots(true).savePageSource(true).includeSelenideSteps(true);
        SelenideLogger.addListener("AllureSelenide", listener);
        open(HOME_PAGE);
        header = new Header();
        cartPage = new CartPage();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Issue("FAST-1312")
    public void verify_product_added_to_cart() {
        open(HOME_PAGE);
        sleep(2000);
        AWESOME_GRANITE_CHIPS_PRODUCT.addToBasket();
        header.shoppingCartBadge.click();
        assertEquals(cartPage.firstProductInCart(),
                "Awesome Granite Chips", "Product is in Cart page");
    }

    @Test(dependsOnMethods = "verify_product_added_to_cart")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_remove_item() {
        assertTrue(cartPage.isTrashButtonDisplayed(), "Trash button is displayed");
        cartPage.trashButton.click();
        assertTrue(cartPage.isFirstProductRemoved(), "The first product is removed from cart");
        open(HOME_PAGE);
    }

    @Test(dependsOnMethods = "verify_product_added_to_cart")
    public void verify_continue_shopping_button() {
        assertTrue(cartPage.isContinueShoppingButtonDisplayed(),
                "Continue Shopping button is displayed");
        assertEquals(cartPage.continueShoppingRedirectURL(), HOME_PAGE + "#/products",
                "Page redirected to Products");
    }

    @Test(dependsOnMethods = "verify_product_added_to_cart")
    @Severity(SeverityLevel.BLOCKER)
    public void verify_checkout_button() {
        assertTrue(cartPage.isCheckoutButtonDisplayed(), "Checkout button is displayed");
        assertEquals(cartPage.checkoutButtonRedirectURL(), HOME_PAGE + "#/checkout-info");
    }

}
