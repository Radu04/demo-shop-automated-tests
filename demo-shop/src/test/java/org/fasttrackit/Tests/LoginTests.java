package org.fasttrackit.Tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.Header;
import org.fasttrackit.pages.HomePage;
import org.fasttrackit.pages.LoginModal;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Feature("Login features")
public class LoginTests {

    HomePage homePage;
    Header header;
    LoginModal loginModal;

    @BeforeClass
    public void before() {
        AllureSelenide listener = new AllureSelenide().screenshots(true).savePageSource(true).includeSelenideSteps(true);
        SelenideLogger.addListener("AllureSelenide", listener);
        open(HOME_PAGE);
        homePage = new HomePage();
        header = new Header();
        loginModal = new LoginModal();
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_is_login_modal_displayed() {
        assertTrue(header.loginButtonIsDisplayed(), "Login button is displayed");
        header.loginButton.click();
        assertTrue(loginModal.isModalDisplayed(), "Login Modal is displayed");
        loginModal.closeModal();
    }

    @Test
    public void verify_close_login_modal() {
        header.loginButton.click();
        sleep(5 * 100);
        assertTrue(loginModal.isCloseButtonDisplayed(), "Close button is displayed");
        loginModal.closeButton.click();
        sleep(5 * 100);
        assertFalse(loginModal.isModalDisplayed(), "Login Modal is not displayed");
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_username_missing_error() {
        header.loginButton.click();
        loginModal.loginButtonFromModal.click();
        assertTrue(loginModal.isErrorMessageDisplayed(), "Error message is displayed");
        assertEquals(loginModal.errorMessageText(), "Please fill in the username!",
                "Username error message is displayed");
        loginModal.closeModal();
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_password_missing_error() {
        header.loginButton.click();
        loginModal.inputUserName.click();
        loginModal.inputUserName.setValue(USERNAME_DINO);
        loginModal.loginButtonFromModal.click();
        assertTrue(loginModal.isErrorMessageDisplayed(), "Error message is displayed");
        assertEquals(loginModal.errorMessageText(), "Please fill in the password!",
                "Password error message is displayed");
        loginModal.closeModal();
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_login_functionality() {
        assertTrue(header.loginButtonIsDisplayed(), "Login button is displayed");
        header.loginButton.click();
        assertTrue(loginModal.isModalDisplayed(), "Login Modal is displayed");
        assertTrue(loginModal.isLoginButtonDisplayed(),
                "Login button from modal is displayed");
        loginModal.inputUserName.click();
        loginModal.inputUserName.setValue(USERNAME_DINO);
        loginModal.inputPassword.click();
        loginModal.inputPassword.setValue(PASSWORD);
        loginModal.loginButtonFromModal.click();
        assertTrue(homePage.isUserNameDisplayed(), "Logged in user");
    }

    @Test(dependsOnMethods = "verify_login_functionality")
    @Severity(SeverityLevel.BLOCKER)
    public void verify_logout_functionality() {
        assertTrue(header.logoutButtonIsDisplayed(), "Logout button is displayed");
        header.logoutButton.click();
        assertTrue(homePage.isGreetingsMessageDisplayed(), "");
        assertEquals(homePage.greetingsMessage(), HELLO_GUEST_GREETINGS_MSG);
    }

}
