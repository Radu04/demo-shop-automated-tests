package org.fasttrackit.Tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Checkout Info features")
public class CheckoutInfoTests {

    CheckoutInfoPage checkoutInfoPage;
    Header header;
    Footer footer;
    CartPage cartPage;

    @BeforeTest
    public void before() {
        AllureSelenide listener = new AllureSelenide().screenshots(true).savePageSource(true).includeSelenideSteps(true);
        SelenideLogger.addListener("AllureSelenide", listener);
        header = new Header();
        cartPage = new CartPage();
        footer = new Footer();
        open(HOME_PAGE);
        checkoutInfoPage = new CheckoutInfoPage();
    }

    @Test
    public void verify_cancel_button() {
        footer.resetButton.click();
        AWESOME_GRANITE_CHIPS_PRODUCT.addToBasket();
        header.shoppingCartBadge.click();
        cartPage.checkoutButton.click();
        assertTrue(checkoutInfoPage.isCancelButtonDisplayed(), "Cancel button is displayed on page.");
        assertEquals(checkoutInfoPage.cancelRedirectURL(), HOME_PAGE + "#/cart");
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_continue_checkout_button() {
        assertTrue(checkoutInfoPage.isContinueCheckoutButtonDisplayed(),
                "Continue Checkout Button is displayed on page.");
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void verify_first_name_missing_info() {
        checkoutInfoPage.continueCheckoutButton.click();
        assertTrue(checkoutInfoPage.isErrorMessageDisplayed(), "Error message is displayed");
        assertEquals(checkoutInfoPage.errorMessageText(), "First Name is required",
                "First name error message is displayed");
    }

    @Test(dependsOnMethods = "verify_first_name_missing_info")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_last_name_missing_info() {
        checkoutInfoPage.inputFirstName.click();
        checkoutInfoPage.inputFirstName.setValue(FIRST_NAME);
        checkoutInfoPage.continueCheckoutButton.click();
        assertTrue(checkoutInfoPage.isErrorMessageDisplayed(), "Error message is displayed");
        assertEquals(checkoutInfoPage.errorMessageText(), "Last Name is required",
                "Last name error message is displayed");
    }

    @Test(dependsOnMethods = "verify_last_name_missing_info")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_address_missing_info() {
        checkoutInfoPage.inputFirstName.click();
        checkoutInfoPage.inputFirstName.setValue(FIRST_NAME);
        checkoutInfoPage.inputLastName.click();
        checkoutInfoPage.inputLastName.setValue(LAST_NAME);
        checkoutInfoPage.continueCheckoutButton.click();
        assertTrue(checkoutInfoPage.isErrorMessageDisplayed(), "Error message is displayed");
        assertEquals(checkoutInfoPage.errorMessageText(), "Address is required",
                "Address error message is displayed");
    }

    @Test(dependsOnMethods = "verify_address_missing_info")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_successful_address_information() {
        checkoutInfoPage.inputFirstName.click();
        checkoutInfoPage.inputFirstName.setValue(FIRST_NAME);
        checkoutInfoPage.inputLastName.click();
        checkoutInfoPage.inputLastName.setValue(LAST_NAME);
        checkoutInfoPage.inputAddress.click();
        checkoutInfoPage.inputAddress.setValue(ADDRESS);
        checkoutInfoPage.continueCheckoutButton.click();
        assertTrue(checkoutInfoPage.isCheckoutSummaryTitleDisplayed(),
                "Input of address information was successful");
    }

}
