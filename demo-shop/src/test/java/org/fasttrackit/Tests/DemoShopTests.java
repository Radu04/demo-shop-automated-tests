package org.fasttrackit.Tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.DataSource;
import org.fasttrackit.pages.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Feature("General features")
public class DemoShopTests {

    HomePage homePage;
    Header header;
    Footer footer;

    @BeforeClass
    public void before() {
        AllureSelenide listener = new AllureSelenide().screenshots(true).savePageSource(true).includeSelenideSteps(true);
        SelenideLogger.addListener("AllureSelenide", listener);
        open(HOME_PAGE);
        homePage = new HomePage();
        header = new Header();
        footer = new Footer();
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    public void verify_greeting_message_is_welcome() {
        assertTrue(homePage.isGreetingsMessageDisplayed(), "Welcome msg must be displayed on page.");
        assertEquals(homePage.greetingsMessage(), HELLO_GUEST_GREETINGS_MSG, "Welcome message is displayed.");
    }

    @Test
    public void verify_homePage_logo() {
        assertTrue(homePage.isLogoDisplayed(), "Logo element exists in navigation bar");
        assertEquals(homePage.logoRedirectURL(), HOME_PAGE, "Clicking the Logo, redirects to homepage");
    }

    @Test
    public void verify_wishlist_button() {
        assertTrue(homePage.isFavouritesIconDisplayed(), "Wish list icon is displayed");
        assertEquals(homePage.wishlistRedirectURL(), HOME_PAGE + "#/wishlist",
                "Page redirected to Wishlist Page");
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void verify_cart_button() {
        assertTrue(homePage.isCartIconDisplayed(), "Cart icon is displayed");
        assertEquals(homePage.cartRedirectURL(), HOME_PAGE + "#/cart",
                "Page redirected to Cart Page");
    }

    @Test(dataProvider = "getProductData", dataProviderClass = DataSource.class)
    public void verify_products_on_homepage(Product product) {
        assertTrue(product.isDisplayed(), "Product " + product.getProductId() + " is displayed on page");
        assertEquals(product.url(), HOME_PAGE + product.getExpectedURL(),
                "Product " + product.getProductId() + " page is opened");
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void cart_icon_updates_upon_adding_a_product_to_the_cart() {
        footer.resetButton.click();
        AWESOME_GRANITE_CHIPS_PRODUCT.addToBasket();
        assertEquals(header.productsInCartBadge(), "1",
                "After adding a product to basket, cart icon is updated");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    public void verify_question_button_opens_help_modal() {
        assertTrue(footer.isQuestionIconDisplayed(), "Question icon is displayed");
        footer.questionIcon.click();
        assertTrue(footer.isHelpModalDisplayed(), "After clicking on help button, help modal is displayed");
        footer.closeButton.click();
    }

    @Test
    public void favourites_icon_updates_upon_adding_a_product_to_the_favourites() {
        AWESOME_METAL_CHAIR_PRODUCT.addToWishList();
        assertTrue(AWESOME_METAL_CHAIR_PRODUCT.isInWishList(),
                "After adding the product to favourites, the favourites icon crashes");
        assertEquals(header.productsInFavouritesBadge(), "1",
                "After adding a product to favourites, the favourites badge updates");
    }

    @Test
    public void verify_sort_mode_products() {
        homePage.sortMode.click();
        homePage.highToLow.click();
        homePage.sortMode.click();
        assertEquals(homePage.firstProduct.getText(), homePage.firstProductAfterSort.getText(),
                "Products are sorting by price from high to low mode");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    public void verify_build_date_demo_shop_footer_text() {
        assertTrue(footer.isBuildDateDisplayed(), "Build date of Demo Shop must be displayed on page");
        assertEquals(footer.buildDateText(), BUILD_DATE_TEXT, "Build date of Demo Shop is displayed");
    }

    @Test
    public void verify_reset_the_application_state_button() {
        assertTrue(footer.isResetStateButtonDisplayed(), "Reset button is displayed");
        footer.resetButton.click();
        assertTrue(header.stateApplicationIsClean(),
                "After clicking on undo button, reset the application state");
    }

}
