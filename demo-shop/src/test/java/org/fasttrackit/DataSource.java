package org.fasttrackit;

import org.fasttrackit.pages.Product;
import org.testng.annotations.DataProvider;

public class DataSource {
    public static final String HELLO_GUEST_GREETINGS_MSG = "Hello guest!";
    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT_ID = "1";
    public static final String AWESOME_METAL_CHAIR_PRODUCT_ID = "3";
    public static final String AWESOME_SOFT_SHIRT_PRODUCT_ID = "5";
    public static final String GORGEOUS_SOFT_PIZZA_PRODUCT_ID = "9";
    public static final String INCREDIBLE_CONCRETE_HAT_PRODUCT_ID = "2";
    public static final String LICENSED_STEEL_GLOVES_PRODUCT_ID = "8";
    public static final String PRACTICAL_METAL_MOUSE_PRODUCT_ID = "7";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_ID = "4";
    public static final String PRACTICAL2_WOODEN_BACON_PRODUCT_ID = "6";
    public static final String REFINED_FROZEN_MOUSE_PRODUCT_ID = "0";

    public static final String BUILD_DATE_TEXT = "Demo Shop | build date 2021-05-21 14:04:30 GTBDT";
    public static final String USERNAME_DINO = "dino"; // normal user
    public static final String PASSWORD = "choochoo";
    public static final String FIRST_NAME = "John";
    public static final String LAST_NAME = "Doe";
    public static final String ADDRESS = "1140 River Rd # 111. New Castle";

    public static final Product AWESOME_GRANITE_CHIPS_PRODUCT = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID);
    public static final Product AWESOME_METAL_CHAIR_PRODUCT = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID);
    public static final Product AWESOME_SOFT_SHIRT_PRODUCT = new Product(AWESOME_SOFT_SHIRT_PRODUCT_ID);
    public static final Product GORGEOUS_SOFT_PIZZA_PRODUCT = new Product(GORGEOUS_SOFT_PIZZA_PRODUCT_ID);
    public static final Product INCREDIBLE_CONCRETE_HAT_PRODUCT = new Product(INCREDIBLE_CONCRETE_HAT_PRODUCT_ID);
    public static final Product LICENSED_STEEL_GLOVES_PRODUCT = new Product(LICENSED_STEEL_GLOVES_PRODUCT_ID);
    public static final Product PRACTICAL_METAL_MOUSE_PRODUCT = new Product(PRACTICAL_METAL_MOUSE_PRODUCT_ID);
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_ID);
    public static final Product PRACTICAL2_WOODEN_BACON_PRODUCT = new Product(PRACTICAL2_WOODEN_BACON_PRODUCT_ID);
    public static final Product REFINED_FROZEN_MOUSE_PRODUCT = new Product(REFINED_FROZEN_MOUSE_PRODUCT_ID);

    @DataProvider(name = "getProductData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[10][];
        products[0] = new Object[]{AWESOME_GRANITE_CHIPS_PRODUCT};
        products[1] = new Object[]{AWESOME_METAL_CHAIR_PRODUCT};
        products[2] = new Object[]{AWESOME_SOFT_SHIRT_PRODUCT};
        products[3] = new Object[]{GORGEOUS_SOFT_PIZZA_PRODUCT};
        products[4] = new Object[]{INCREDIBLE_CONCRETE_HAT_PRODUCT};
        products[5] = new Object[]{LICENSED_STEEL_GLOVES_PRODUCT};
        products[6] = new Object[]{PRACTICAL_METAL_MOUSE_PRODUCT};
        products[7] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT};
        products[8] = new Object[]{PRACTICAL2_WOODEN_BACON_PRODUCT};
        products[9] = new Object[]{REFINED_FROZEN_MOUSE_PRODUCT};
        return products;
    }

}
