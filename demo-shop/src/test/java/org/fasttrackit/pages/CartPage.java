package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    public final SelenideElement trashButton = $("[data-icon=trash]");
    public final SelenideElement continueShoppingButton = $(".btn.btn-danger");
    public final SelenideElement checkoutButton = $(".btn.btn-success");

    private final SelenideElement firstProductInCart = $("#item_1_title_link");

    public String firstProductInCart() {
        return firstProductInCart.getText();
    }

    public String continueShoppingRedirectURL() {
        return continueShoppingButton.getAttribute("href");
    }

    public String checkoutButtonRedirectURL() {
        return checkoutButton.getAttribute("href");
    }

    public boolean isTrashButtonDisplayed() {
        return trashButton.exists() && trashButton.isDisplayed();
    }

    public boolean isFirstProductRemoved() {
        return !firstProductInCart.exists() && !firstProductInCart.isDisplayed();
    }

    public boolean isContinueShoppingButtonDisplayed() {
        return continueShoppingButton.exists() && continueShoppingButton.isDisplayed();
    }

    public boolean isCheckoutButtonDisplayed() {
        return checkoutButton.exists() && checkoutButton.isDisplayed();
    }

}
