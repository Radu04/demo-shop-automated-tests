package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    public final SelenideElement shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");
    public final SelenideElement favouritesIcon = $("[data-icon=heart]~.shopping_cart_badge");
    public final SelenideElement loginButton = $(".fa-sign-in-alt");
    public final SelenideElement logoutButton = $(".fa-sign-out-alt");

    public String productsInCartBadge() {
        return shoppingCartBadge.getText();
    }

    public String productsInFavouritesBadge() {
        return favouritesIcon.getText();
    }

    public boolean stateApplicationIsClean() {
        return !shoppingCartBadge.exists() && !shoppingCartBadge.isDisplayed() &&
                !favouritesIcon.exists() && !favouritesIcon.isDisplayed();
    }

    public boolean loginButtonIsDisplayed() {
        return loginButton.exists() && loginButton.isDisplayed();
    }

    public boolean logoutButtonIsDisplayed() {
        return logoutButton.exists() && logoutButton.isDisplayed();
    }

}
