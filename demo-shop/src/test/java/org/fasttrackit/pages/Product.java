package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product {

    private final SelenideElement title;
    private final SelenideElement mainProductCart;
    private final SelenideElement favouritesButton;
    private final SelenideElement parent;
    private final SelenideElement addToBasketIcon;

    private final String expectedURL;
    private final String productId;

    public Product(String productId) {
        this.title = $(format("a[href='#/product/%s']", productId));
        this.mainProductCart = title.parent().parent();
        this.favouritesButton = mainProductCart.$("[data-icon*=heart]");
        this.parent = title.parent().parent();
        this.addToBasketIcon = parent.$("[data-icon=cart-plus]");
        this.expectedURL = format("#/product/%s", productId);
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public boolean isDisplayed() {
        return title.exists() && title.isDisplayed();
    }

    public String url() {
        return title.getAttribute("href");
    }

    public String getExpectedURL() {
        return this.expectedURL;
    }

    public void addToBasket() {
        addToBasketIcon.click();
    }

    public void addToWishList() {
        favouritesButton.click();
    }

    public boolean isInWishList() {
        return favouritesButton.getAttribute("data-icon").equals("heart-broken");
    }

}
