package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutInfoPage {

    public final SelenideElement cancelButton = $(".btn.btn-danger");
    public final SelenideElement continueCheckoutButton = $(".btn.btn-success");
    public final SelenideElement errorMessage = $(".error");
    public final SelenideElement inputFirstName = $("#first-name");
    public final SelenideElement inputLastName = $("#last-name");
    public final SelenideElement inputAddress = $("#address");
    public final SelenideElement checkoutSummaryTitle = $(".text-muted");

    public boolean isCancelButtonDisplayed() {
        return cancelButton.exists() && cancelButton.isDisplayed();
    }

    public boolean isContinueCheckoutButtonDisplayed() {
        return continueCheckoutButton.exists() && continueCheckoutButton.isDisplayed();
    }

    public boolean isErrorMessageDisplayed() {
        return errorMessage.exists() && errorMessage.isDisplayed();
    }

    public boolean isCheckoutSummaryTitleDisplayed() {
        return checkoutSummaryTitle.exists() && checkoutSummaryTitle.isDisplayed();
    }

    public String cancelRedirectURL() {
        return cancelButton.getAttribute("href");
    }

    public String errorMessageText() {
        return errorMessage.getText();
    }

}
