package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {

    private final SelenideElement loginModal = $(".modal-content");

    public final SelenideElement loginButtonFromModal = $(".btn-primary");
    public final SelenideElement inputUserName = $("#user-name");
    public final SelenideElement inputPassword = $("#password");
    public final SelenideElement errorMessage = $(".error");
    public final SelenideElement closeButton = $(".close");

    public boolean isModalDisplayed() {
        return loginModal.exists() && loginModal.isDisplayed();
    }

    public boolean isLoginButtonDisplayed() {
        return loginButtonFromModal.exists() && loginButtonFromModal.isDisplayed();
    }

    public boolean isErrorMessageDisplayed() {
        return errorMessage.exists() && errorMessage.isDisplayed();
    }

    public boolean isCloseButtonDisplayed() {
        return closeButton.exists() && closeButton.isDisplayed();
    }

    public String errorMessageText() {
        return errorMessage.getText();
    }

    public void closeModal() {
        if (closeButton.exists() && closeButton.isDisplayed()) {
            closeButton.click();
        }
    }

}
