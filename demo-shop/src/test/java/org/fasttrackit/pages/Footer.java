package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {

    public final SelenideElement questionIcon = $(".fa-question");
    public final SelenideElement resetButton = $(".fa-undo");
    public final SelenideElement closeButton = $(".modal-content [aria-hidden=true]");

    private final SelenideElement helpModal = $(".modal-content");
    private final SelenideElement buildDate = $(".nav-link");

    public String buildDateText() {
        return buildDate.getText();
    }

    public boolean isQuestionIconDisplayed() {
        return questionIcon.exists() && questionIcon.isDisplayed();
    }

    public boolean isHelpModalDisplayed() {
        return helpModal.exists() && helpModal.isDisplayed();
    }

    public boolean isBuildDateDisplayed() {
        return buildDate.exists() && buildDate.isDisplayed();
    }

    public boolean isResetStateButtonDisplayed() {
        return resetButton.exists() && resetButton.isDisplayed();
    }

}
