package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePage {

    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final SelenideElement wishlistIcon = $(".navbar-nav [data-icon=heart]");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final ElementsCollection allProducts = $$(".card");
    private final ElementsCollection allProductsAfterSort = $$(".card");

    public final SelenideElement firstProduct = allProducts.first();
    public final SelenideElement sortMode = $(".sort-products-select");
    public final SelenideElement highToLow = $("option[value=hilo]");
    public final SelenideElement firstProductAfterSort = allProductsAfterSort.first();
    public final SelenideElement userName = $("[href='#/account']");

    /**
     * Page content
     */
    public String greetingsMessage() {
        return greetings.getText();
    }

    public String logoRedirectURL() {
        return logo.parent().getAttribute("href");
    }

    public String wishlistRedirectURL() {
        return wishlistIcon.parent().getAttribute("href");
    }

    public String cartRedirectURL() {
        return cartIcon.parent().getAttribute("href");
    }

    /**
     * Verifiers
     */
    public boolean isGreetingsMessageDisplayed() {
        return greetings.exists() && greetings.isDisplayed();
    }

    public boolean isLogoDisplayed() {
        return logo.exists() && logo.isDisplayed();
    }

    public boolean isFavouritesIconDisplayed() {
        return wishlistIcon.exists() && wishlistIcon.isDisplayed();
    }

    public boolean isCartIconDisplayed() {
        return cartIcon.exists() && cartIcon.isDisplayed();
    }

    public boolean isUserNameDisplayed(){
        return userName.exists() && userName.isDisplayed();
    }

}
